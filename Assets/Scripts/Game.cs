﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private bool _timerIsOn;
    [SerializeField] private float _timerValue;
    [SerializeField] private Text _timerView;

    [Header("Objects")]
    [SerializeField] private Player _player;
    [SerializeField] private Exit _exitFromLevel;
    [SerializeField] private Door _door;

    private float _timer = 0;
    private static int coins = 0;

    private bool _gameIsEnded = false;
    private bool _gameIsWin = false;
    public GameObject _timerText;
    public GameObject _winText;
    public GameObject _loseText;
    public Text _coinsText;


    private void Awake()
    {
        _timer = _timerValue;
    }

    private void Start()
    {
        _exitFromLevel.Close();
    }

    private void Update()
    { 
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (_gameIsWin)
        {
            var index = SceneManager.GetActiveScene().buildIndex;
            if (Input.GetKey(KeyCode.Return))
            {
                SceneManager.LoadScene(index + 1);
            }
        }
        if (_gameIsEnded)
            return;

        _coinsText.text = $"Собрано монеток: {coins}";

        TimerTick();
        LookAtPlayerHealth();
        LookAtPlayerInventory();
        TryCompleteLevel();
    }

    private void TimerTick()
    {
        if(_timerIsOn == false)
            return;
        
        _timer -= Time.deltaTime;
        _timerView.text = $"{_timer:F1}";
        
        if(_timer <= 0)
            Lose();
    }
    private void TryCompleteLevel()
    {
        if (_exitFromLevel.IsOpen == false)
            return;

        var flatExitPosition = new Vector2(_exitFromLevel.transform.position.x, _exitFromLevel.transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);

        if (flatExitPosition == flatPlayerPosition)
            Victory();
            
    }

    private void LookAtPlayerHealth()
    {
        if(_player.IsAlive)
            return;

        float rotateSpeed = 120;
        _player.transform.RotateAround(_player.transform.position, Vector3.up, rotateSpeed * Time.deltaTime);
        if (_player.transform.localScale.x > 0)
        {
            _player.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f) * Time.deltaTime * 15;
        }
        else
        {
            Lose();
            Destroy(_player.gameObject);
        }
}

    private void LookAtPlayerInventory()
    {
        if (_player.HasKey)
        {
            _exitFromLevel.Open();
        }
        if (_player.HasKeyDoor)
        {
            _door.Open();
        }
        if (_player.HasCoin)
        {
            coins++;
            _player.HasCoin = false;
        }
        
    }

    public void Victory()
    {
        _gameIsEnded = true;
        _gameIsWin = true;
        _timerText.SetActive(false);
        _winText.SetActive(true);
        _player.Disable();
        PlayerPrefs.SetInt("CoinCount", coins);
        PlayerPrefs.Save();
    }

    public void Lose()
    {
        _timerText.SetActive(false);
        _loseText.SetActive(true);
        _gameIsEnded = true;
        _player.Disable();
        coins = 0;
    }
}
