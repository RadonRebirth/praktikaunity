using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EndScoreShow : MonoBehaviour
{
    public Text _endText;
    int coins;

    private void Start()
    {
        coins = PlayerPrefs.GetInt("CoinCount", 0);
    }

    void Update()
    {
        _endText.text = $"�� ������� {coins} �������!";
    }
}
