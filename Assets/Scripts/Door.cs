﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool IsOpen { get; private set; }

    private void Update()
    {
        if (IsOpen)
        {
            Destroy(gameObject);
        }
    }
    public void Open()
    {
        IsOpen = true;
    }
}
