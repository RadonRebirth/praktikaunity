﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRender : MonoBehaviour
{
    [Header("Field of View")]
    [SerializeField] private GameObject _fieldOfView;
    [SerializeField] public LineRenderer _line;
    void Update()
    {
        _line.SetPosition(0, transform.position);
        _line.SetPosition(1, _fieldOfView.transform.position);
    }
}
