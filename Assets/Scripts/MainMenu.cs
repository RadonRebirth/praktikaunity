using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ToggleFullscreen(bool isFullscreen)
    {
        if (isFullscreen != Screen.fullScreen)
        {
            Screen.fullScreen = isFullscreen;
        }
        else
        {
            Screen.fullScreen = !isFullscreen;
        }
    }


    public void QuitGame()
    {
        Application.Quit();
    }
}
